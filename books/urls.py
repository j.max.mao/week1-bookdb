from django.urls import path

from books.views import show_books

urlpatterns = [
    path("books/", show_books),
]